package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.P1Beans;
import dao.UsermanagementDao;

/**
 * Servlet implementation class Project1userichiranServlet
 */
@WebServlet("/Project1userichiranServlet")
public class Project1userichiranServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Project1userichiranServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		P1Beans userInfokey= (P1Beans)session.getAttribute("userInfokey");

		if(userInfokey==null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}


				// ユーザ一覧情報を取得
				UsermanagementDao userDao = new UsermanagementDao();
				List<P1Beans> userList = userDao.findAllExcept管理者();


				// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("userListkey管理者以外", userList);



				// ユーザ一覧のjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ユーザー一覧.jsp");
				dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 request.setCharacterEncoding("UTF-8");
		String loginID = request.getParameter("loginID");
		String userNAME = request.getParameter("userNAME");
		String dateSTART = request.getParameter("dateSTART");
		String dateEND = request.getParameter("dateEND");

		// ユーザー検索
		// ユーザ一覧情報を取得
		UsermanagementDao userDao = new UsermanagementDao();
		List<P1Beans> userResult = userDao.findSearch(loginID,userNAME,dateSTART,dateEND);

		request.setAttribute("userListkey管理者以外", userResult);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ユーザー一覧.jsp");
		dispatcher.forward(request, response);

	}

}
