package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.P1Beans;
import dao.UsermanagementDao;

/**
 * Servlet implementation class P1削除ボタン
 */
@WebServlet("/P1delete")
public class P1削除ボタン extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public P1削除ボタン() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		P1Beans userInfokey= (P1Beans)session.getAttribute("userInfokey");

		if(userInfokey==null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}

		String idd = request.getParameter("id");

		// ユーザ一覧情報を取得
		UsermanagementDao userDao = new UsermanagementDao();
		P1Beans user = userDao.findById(idd);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userListkey", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ユーザ削除確認.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// マルチバイト文字列の文字化け対応
		request.setCharacterEncoding("UTF-8");

		// TODO URLパラメータから値を取得
		//		String all = request.getParameter("all");            //このようにget.Parameterで広くとってきたらどうすればいいか(all=${userListkey}の場合)？？？
		//        P1Beans all.getLogin_id();

		String loginId = request.getParameter("loginId");
		UsermanagementDao userDao = new UsermanagementDao();
		int count = userDao.delete(loginId);

		/** 削除に成功した場合 **/
		if (count != 0) {
			request.setAttribute("Msg", "情報削除に成功しました");

//			response.sendRedirect("Project1userichiranServlet");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ユーザ削除確認.jsp");
			dispatcher.forward(request, response);

		/** 削除に失敗した場合 **/
		} else {
			//		HttpSession errmsgSession2 = request.getSession();
			//		errmsgSession2.setAttribute("errMsg", "削除できませんでした");
			request.setAttribute("errMsg", "削除できませんでした。");

			String idd = request.getParameter("id");

			// ユーザ一覧情報を取得
			userDao = new UsermanagementDao();
			P1Beans user = userDao.findById(idd);

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("userListkey", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ユーザ削除確認.jsp");
			dispatcher.forward(request, response);

		}

	}

}
