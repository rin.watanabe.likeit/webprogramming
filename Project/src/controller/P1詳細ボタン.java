package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.P1Beans;
import dao.UsermanagementDao;

/**
 * Servlet implementation class P1詳細ボタンの先
 */
@WebServlet("/P1details")
public class P1詳細ボタン extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public P1詳細ボタン() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		P1Beans userInfokey= (P1Beans)session.getAttribute("userInfokey");

		if(userInfokey==null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;               //break; みたいなもの
		}

		String id = request.getParameter("id");


		// ユーザ一覧情報を取得
		UsermanagementDao userDao = new UsermanagementDao();
		P1Beans user = userDao.findById(id);                        //型を List<P1Beans> にしない     userの束はList

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userkeydetails", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ユーザ情報詳細参照.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		doGet(request, response);
//	}

}
