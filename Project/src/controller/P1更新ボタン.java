package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.P1Beans;
import dao.UsermanagementDao;

/**
 * Servlet implementation class P1更新ボタン
 */
@WebServlet("/P1update")
public class P1更新ボタン extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public P1更新ボタン() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
        HttpSession session = request.getSession();
        P1Beans userInfokey= (P1Beans)session.getAttribute("userInfokey");

		if(userInfokey == null) {     //ここでsession.getAttribute("userInfokey")== としようとするとできない。ちゃんと型に入れて変数にいれる！
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");  //また、サーブレットに送るとエラーになる。jspだけいける
			dispatcher.forward(request, response);
		}

		String iid = request.getParameter("id");


		// ユーザ一覧情報を取得
		UsermanagementDao userDao = new UsermanagementDao();
		P1Beans user = userDao.findById(iid);                                //型を List<P1Beans> にしない   userの束がList

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userkeyup", user);

//		HttpSession session = request.getSession();
//		session.setAttribute("userkeyup",user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ユーザ情報更新.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		// このクラスのセッションパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");

		//HttpSession session = request.getSession();
		//P1Beans seData= (P1Beans)request.getAttribute("userkeyup");                //このやり方でなぜかできない。doGetからseDataが取れない-(同じ)サーブレット内でセットしたセッションは使えるのか？？？
		//seData.getLogin_id();       loginIdと同じ                                               //セッションでもリクエストでもできない


		String newPassword = request.getParameter("newPassword");
		String newPassword2 = request.getParameter("newPassword2");
		String newName = request.getParameter("newName");
		String newDate = request.getParameter("newDate");

		int count;
		UsermanagementDao userDao;
		HttpSession session = request.getSession();
	    P1Beans userInfokey= (P1Beans)session.getAttribute("userInfokey");

		/** パスワードの入力がない場合 **/
		//if (newPassword == null || newPassword.equals("") && newPassword2 == null || newPassword2.equals("")) {
		if (newPassword.equals("") && newPassword2.equals("")) {
//			newPassword = seData.getPassword();                  //doGetからのセッションが取れた場合に可能
//			userDao = new UsermanagementDao();                   //パスワードの不一致に入る
//			count = userDao.update(newPassword, newName, newDate, seData.getLogin_id());

			userDao = new UsermanagementDao();
			count = userDao.update2(newName, newDate, loginId);
			}

		//P1Beans pb = new P1Beans(newPassword, newName, newDate);  Beansにいれる必要ない

		/** ノーマルな更新 **/
		else{
			userDao = new UsermanagementDao();
			count = userDao.update(newPassword, newName, newDate, loginId);
		}


		/** 更新に失敗した場合 **/
    	if(!(newPassword.equals(newPassword2))) {                                            //String型を比較するには a.equals(b)
//    		HttpSession errmsgSession = request.getSession();
//    		errmsgSession.setAttribute("errMsg", "入力された内容は正しくありません(パスワードの不一致)");
    		request.setAttribute("errMsg", "入力された内容は正しくありません(パスワードの不一致)");


    		String iid = request.getParameter("id");

    		//UsermanagementDao userDao2 = new UsermanagementDao();
    		P1Beans user = userDao.findById(iid);

    		session = request.getSession();
    		session.setAttribute("userkeyup",user);                  //このクラスのdoGetメソッドをここでもやると入力画面の最初の状態に戻れる


    		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ユーザ情報更新.jsp");
    		dispatcher.forward(request, response);


    	/** 更新に成功した場合 **/
    	}else if(count != 0) {
//    		HttpSession msgSession = request.getSession();
//    		msgSession.setAttribute("Msg", "情報更新に成功しました");
    		request.setAttribute("Msg", "情報更新に成功しました");

    		// ユーザ一覧情報を取得
			//userDao = new UsermanagementDao();
			List<P1Beans> userList = userDao.findAllExcept管理者();


			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("userListkey管理者以外", userList);


    		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ユーザー一覧.jsp");
    		dispatcher.forward(request, response);
//    		                or
//   		response.sendRedirect("Project1userichiranServlet");


    	/** 更新に失敗した場合 **/
    	}else {
//    		HttpSession errmsgSession2 = request.getSession();
//    		errmsgSession2.setAttribute("errMsg", "入力された内容は正しくありません(入力項目にパスワード以外で未入力がある)");
    		request.setAttribute("errMsg", "入力された内容は正しくありません(入力項目にパスワード以外で未入力がある)");


    		String iid = request.getParameter("id");

    		//UsermanagementDao userDao2 = new UsermanagementDao();
    		P1Beans user = userDao.findById(iid);

    		session = request.getSession();
    		session.setAttribute("userkeyup",user);                  //このクラスのdoGetメソッドをここでもやる


    		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ユーザ情報更新.jsp");
    		dispatcher.forward(request, response);

    	}


	}

}
