package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.P1Beans;
import dao.UsermanagementDao;


/**
 * Servlet implementation class 新規登録
 */
@WebServlet("/新規登録")
public class 新規登録 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public 新規登録() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		P1Beans userInfokey= (P1Beans)session.getAttribute("userInfokey");

		if(userInfokey==null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ユーザ新規登録.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		   request.setCharacterEncoding("UTF-8");


			// リクエストパラメータの入力項目を取得
			    String loginid0 = request.getParameter("loginid0");
				String password0 = request.getParameter("password0");
				String password02 = request.getParameter("password02");
				String name0 = request.getParameter("name0");
				String date0 = request.getParameter("date0");


//				SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd");        // SimpleDateFormatクラス
//		            Date date = null;
//					try {
//						date = (Date) sdFormat.parse(date0);           //java.util.Date date = sdFormat.parse(date0);      どっちが正しいのか？？？
//					} catch (ParseException e) {
//						// TODO 自動生成された catch ブロック
//						e.printStackTrace();
//					}

//				P1Beans pb = new P1Beans(loginid0, password02, name0, date);

				UsermanagementDao userDao = new UsermanagementDao();

				int count = userDao.insert2(loginid0, password0, name0, date0);
//		    	int count = userDao.insert(pb);               //()にいれる引数の形によって次に行くDAOの引数が変わる。

		    	/** 登録に失敗した場合 **/
		    	if(!(password0.equals(password02))) {     //String型を比較するには a.equals(b)
		    		request.setAttribute("errMsg", "入力された内容は正しくありません(パスワードの不一致)");

		    		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ユーザ新規登録.jsp");
		    		dispatcher.forward(request, response);

		    	/** 登録に成功した場合 **/
		    	}else if(count != 0) {       //nullだとうまく行かないから0にした。

		    		// ユーザ一覧情報を取得
					UsermanagementDao userDao2 = new UsermanagementDao();
					List<P1Beans> userList = userDao2.findAllExcept管理者();


					// リクエストスコープにユーザ一覧情報をセット
					request.setAttribute("userListkey管理者以外", userList);

					request.setAttribute("Msg", "新規登録に成功しました");


		    		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ユーザー一覧.jsp");
		    		dispatcher.forward(request, response);
//		    		                or
		    		//response.sendRedirect("Project1userichiranServlet");

		    	/** 登録に失敗した場合 **/
		    	}else {
		    		// リクエストスコープにエラーメッセージをセット
	    			request.setAttribute("errMsg", "入力された内容は正しくありません(入力項目に未入力があるかログインIDが既に登録されている)");

	    		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ユーザ新規登録.jsp");
	    		dispatcher.forward(request, response);

		    	}

//		    	//既に登録されているログインIDが入力された場合、未入力がある場合
//	    	    ログインIDはUNIQUEだからSQLに登録されないつまりcount=null、入力項目が１つでも未入力だと登録されない(??)つまりcount=null(elseと同じ)
//



	}

}
