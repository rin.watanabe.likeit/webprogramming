package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import beans.P1Beans;

public class UsermanagementDao {

	public P1Beans findByLoginInfo(String loginId, String password) {
		Connection con = null;

		try {
			// データベースへ接続
			con = DBmanager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id =? and password =?";

			String algPassword = algorithm(password);

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, algPassword);
			ResultSet rs = pStmt.executeQuery(); //フォームに入力したのがLoginServletを通ってここでDBへ行く.
													//?にマッチしたテーブルの＊(全部)を取り出してrsに入れてる。

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int Id = rs.getInt("id");
			String loginIdData = rs.getString("login_id"); //次のユーザー一覧画面で使うlogin_idカラムの値、nameカラムの値を共に文字列として取り出す。
			String nameData = rs.getString("name");
			return new P1Beans(Id, loginIdData, nameData); //結局findByLoginInfoから出た時にはこの２つを持ってる。

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					//return null;
				}
			}
		}
	}




	public List<P1Beans> findSearch(String loginIdP, String nameP, String dateS, String dateE) {
		Connection con = null;
		List<P1Beans> userList = new ArrayList<P1Beans>();

		try {
			// データベースへ接続
			con = DBmanager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE login_id != 'admin'";

			if(!loginIdP.equals("")) {
				sql += " AND login_id = '" + loginIdP + "'";
			}

			if(!nameP.equals("")) {
				sql += " AND name LIKE '%" + nameP + "%'";
			}

			if(!dateS.equals("")) {
				sql += " AND birth_date >='" + dateS + "'";
			}

			if(!dateE.equals("")) {
				sql += " AND birth_date <='" + dateE + "'";
			}

			// SELECTを実行し、結果表を取得
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");

				P1Beans user = new P1Beans(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}



	public P1Beans findById(String id2) {     //SQLに行くだけなら全部Stringで大丈夫(型の形関係ない)   //型を List<P1Beans> にしない
		Connection con = null;

		P1Beans user = null;

		int id = 0;
		String loginId = null;
		String name = null;
		Date birthDate = null;               //   SimpleDateFormatクラスでDate型に変える
		//  Date date = null;
		String password = null;
		String createDate = null;
		String updateDate = null;

		try {
			// データベースへ接続
			con = DBmanager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id2);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}


//	見本		// 必要なデータのみインスタンスのフィールドに追加
//			String loginIdData = rs.getString("login_id"); //次のユーザー一覧画面で使うlogin_idカラムの値、nameカラムの値を共に文字列として取り出す。
//			String nameData = rs.getString("name");
//			return new P1Beans(loginIdData, nameData); //結局findByLoginInfoから出た時にはこの２つを持ってる。


			// 必要なデータのみインスタンスのフィールドに追加
			//while (rs.next()) {             データは１つしか取れないからwhileで回さなくていい
				id = rs.getInt("id");
				loginId = rs.getString("login_id");
				name = rs.getString("name");
				birthDate = rs.getDate("birth_date");
				password = rs.getString("password");
				createDate = rs.getString("create_date");
				updateDate = rs.getString("update_date");

//				SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy年MM月dd日");        // SimpleDateFormatクラス
//	            //Date date = null;      returnのとこで使えるように上に移動
//				try {
//					date = (Date) sdFormat.parse(birthDate);           //java.util.Date date = sdFormat.parse(birthDate);      どっちが正しいのか？？？
//				} catch (ParseException e) {
//					// TODO 自動生成された catch ブロック
//					e.printStackTrace();                      rsからデータを取る時にSQLの方でもうDate型に入っちゃってるからgetDateでとれる、だからこれで変える必要ない
//				}

//				if(id == 0) {        //これいる？？？  - if (!rs.next()) {
//					return null;                              return null; } でふるいにかけられてるからいらない
//				}

				user = new P1Beans(id, loginId, name, birthDate, password, createDate, updateDate);

				//userList.add(user);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}

		}
		return user;
	}



	public List<P1Beans> findAll() {
		Connection con = null;
		List<P1Beans> userList = new ArrayList<P1Beans>();

		try {
			// データベースへ接続
			con = DBmanager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user";

			// SELECTを実行し、結果表を取得
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");

				P1Beans user = new P1Beans(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}



	public List<P1Beans> findAllExcept管理者() {
		Connection con = null;
		List<P1Beans> userList = new ArrayList<P1Beans>();

		try {
			// データベースへ接続
			con = DBmanager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id <> 1";

			// SELECTを実行し、結果表を取得
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");

				P1Beans user = new P1Beans(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}



	public int insert(P1Beans pb) {
		Connection con = null;
		PreparedStatement stmt = null;
		int count;

		try {
			// データベース接続
			con = DBmanager.getConnection();
			// 実行SQL文字列定義
			String insertSQL = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,now(),now())";
			// ステートメント生成
			stmt = con.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定

			//idは勝手に決まるからいらない
			stmt.setString(1, pb.getLogin_id());
			stmt.setString(2, pb.getName());
			stmt.setDate(3, pb.getBirth_date());
			stmt.setString(4, pb.getPassword());

			// 登録SQL実行
			count = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;          //nullだとうまく行かないから0
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return 0;
			}
		}
		return count;
	}



	public int insert2(String loginid0, String password0, String name0, String date0) {        //SQLに行くだけなら全部Stringで大丈夫(型の形関係ない)

		Connection con = null;
		PreparedStatement stmt = null;
		int count;

		try {
			// データベース接続
			con = DBmanager.getConnection();
			// 実行SQL文字列定義
			String insertSQL = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,now(),now())";
			// ステートメント生成
			stmt = con.prepareStatement(insertSQL);


			String algPassword0 = algorithm(password0);


			// SQLの?パラメータに値を設定
			//idは勝手に決まるからいらない
			stmt.setString(1, loginid0);
			stmt.setString(2, name0);
			stmt.setString(3, date0);
			stmt.setString(4, algPassword0);

			// 登録SQL実行
			count = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;                       //nullだとうまく行かないから0
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return 0;
			}
		}
		return count;
	}



	public int update(String newPassword, String newName, String newDate, String login_id) {
		Connection con = null;
		PreparedStatement stmt = null;
		int count;

		try {
			// データベース接続
			con = DBmanager.getConnection();
			// 実行SQL文字列定義
			String updateSQL = "UPDATE user SET password = ?, name = ?, birth_date = ? WHERE login_id = ?;";
			// ステートメント生成
			stmt = con.prepareStatement(updateSQL);

			String algNewPassword = algorithm(newPassword);

			stmt.setString(1, algNewPassword);
			stmt.setString(2, newName);
			stmt.setString(3, newDate);
			stmt.setString(4, login_id);
			// 登録SQL実行
			count = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return 0;
			}
		}
		return count;

	}



	public int update2(String newName, String newDate, String login_id) {
		Connection con = null;
		PreparedStatement stmt = null;
		int count;

		try {
			// データベース接続
			con = DBmanager.getConnection();
			// 実行SQL文字列定義
			String updateSQL2 = "UPDATE user SET name = ?, birth_date = ? WHERE login_id = ?;";
			// ステートメント生成
			stmt = con.prepareStatement(updateSQL2);

			stmt.setString(1, newName);
			stmt.setString(2, newDate);
			stmt.setString(3, login_id);
			// 登録SQL実行
			count = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return 0;
			}
		}
		return count;

	}



	public int delete(String login_id) {
		Connection con = null;
		PreparedStatement stmt = null;
		int count;

		try {
			// データベース接続
			con = DBmanager.getConnection();
			// 実行SQL文字列定義
			String deleteSQL = "DELETE FROM user WHERE login_id = ?;";
			// ステートメント生成
			stmt = con.prepareStatement(deleteSQL);
			stmt.setString(1, login_id);

			// 登録SQL実行
			count = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return 0;
			}
		}
		return count;

	}



	public String algorithm(String alg) {
		String source = alg;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return null;
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		return result;

	}

}
