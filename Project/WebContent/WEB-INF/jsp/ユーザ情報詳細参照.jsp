 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ユーザ情報詳細参照</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="css/style.css" rel="stylesheet" type="text/css" />

</head>

<body>
    <header>
        <p class="right row">
            ${sessionScope.userInfokey.name}さん
            <a class="red" href="P1LogoutServlet">ログアウト</a>
        </p>
    </header>


    &nbsp;


    <h1 class="text-center">ユーザ情報詳細参照</h1>


    &nbsp;


    <form class=”ex3”>

<%--  <c:forEach var="user" items="${userkeydetails}" >  --%>
        <div class="col-9 row">
            <label class="col-form-label col-2 mx-auto">ログインID</label>

            <label for="exampleInputPassword1" style="text-align:center" class="col-3">${requestScope.userkeydetails.login_id }</label>
        </div>
        <div class="col-9 row">
            <label class="col-form-label col-2 mx-auto">ユーザ名</label>

            <label for="exampleInputPassword1" style="text-align:center" class="col-3">${userkeydetails.name }</label>
        </div>
        <div class="col-9 row">
            <label class="col-form-label col-2 mx-auto">生年月日</label>

            <label for="exampleInputPassword1" style="text-align:center" class="col-3">${userkeydetails.birth_date}</label>
        </div>
        <div class="col-9 row">
            <label class="col-form-label col-2 mx-auto">登録日時</label>

            <label for="exampleInputPassword1" style="text-align:center" class="col-3">${userkeydetails.create_date}</label>
        </div>
        <div class="col-9 row">
            <label class="col-form-label col-2 mx-auto">更新日時</label>

            <label for="exampleInputPassword1" style="text-align:center" class="col-3">${userkeydetails.update_date}</label>
        </div>
<%--         </c:forEach> --%>

    </form>


    &nbsp;


    <div class="col-sm-2" style="text-align:center">
        <a href="Project1userichiranServlet">戻る</a>
    </div>


</body></html>