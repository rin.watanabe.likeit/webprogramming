 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ユーザ削除確認</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="css/style.css" rel="stylesheet" type="text/css" />

</head>

<body>
    <header>
		<p class="right row">
			${sessionScope.userInfokey.name}さん <a class="red" href="P1LogoutServlet">ログアウト</a>      
		</p>
	</header>


    &nbsp;


    <h1 class="text-center">ユーザ削除確認</h1>


    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>


    &nbsp;


    <div class="col-8">
        <label class="col-2 mx-auto">ログインID:</label>
        <label class="col-sm-2">${userListkey.login_id}</label>
        <p class="col-sm-8">を本当に削除してもよろしいでしょうか。</p>

    </div>


    <div class="sk col-sm-10 form-row center-block text-center mx-auto">

    <div class="col-sm-6 text-center">
         <button onclick="location.href='Project1userichiranServlet'" class="col-5 btn-primary btn-lg" style="text-align:center" type="submit">キャンセル</button>
    </div>

    <div class="col-sm-6 text-center">
    <form method="post">
         <input type="hidden" value="${userListkey.login_id}" name="loginId">
         <button onclick="location.href='P1delete?loginId=${userListkey.login_id}'" class="col-5 btn-primary btn-lg" style="text-align:center">OK</button>   <%-- これだけだとできないからinputタグをプラス --%>      
    </form>
    </div>

    </div>

    </body></html>

