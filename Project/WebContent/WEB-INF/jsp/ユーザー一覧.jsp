<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザー一覧</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/style.css" rel="stylesheet" type="text/css" />        <!-- cssフォルダーをWebContentの一個下に置き style.cssの前に css/ をかく！ -->

</head>

<body>



	<header>
		<p class="right row">
			${sessionScope.userInfokey.name}さん <a class="red" href="P1LogoutServlet">ログアウト</a>
		</p>
	</header>


    <c:if test="${Msg != null}" >
	    <div class="alert alert-danger" role="success">${Msg}</div>
	</c:if>


	&nbsp;


	<h1 class="col-sm-3 mx-auto">ユーザー一覧</h1>


	<p class="right mx-auto">
		<a href="新規登録">新規登録</a>
	</p>


	<form action="Project1userichiranServlet" method="post">

		<div class="col-sm-10 mx-auto">
			<div class="form-group row">
				<label for="exampleInputEmail1" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-5">
					<input name="loginID" type="text" class="form-control" id="exampleInputEmail1"
						aria-describedby="emailHelp" placeholder="ID">
				</div>
			</div>
			<div class="form-group row">
				<label for="exampleInputPassword1" class="col-sm-2">ユーザ名</label>
				<div class="col-sm-5">
					<input name="userNAME" type="text" class="form-control"
						id="exampleInputPassword1" placeholder="名">
				</div>
			</div>
		</div>


		<div class="col-sm-10 mx-auto">
			<div class="form-group row" id="datepicker-daterange">
				<label class="col-sm-2 control-label">生年月日</label>
				<div class="col-sm-5 form-inline">
					<div class="input-daterange input-group" id="datepicker">
						<input type="date" class="input-sm form-control" name="dateSTART"
							placeholder="年/月/日" /> <span class="input-group-addon col-sm-1">
							〜 </span> <input type="date" class="input-sm form-control" name="dateEND"
							placeholder="年/月/日" />
					</div>
				</div>
			</div>
		</div>


		<div class="col-sm-10 mx-auto">
			<p class="right">
				<%--a href="Project1userichiranServlet?requestValue=${rekey }"--%>
				<button type="submit" class="btn btn-primary col-sm-8">検索</button>
			</p>
		</div>

	</form>

	<hr size="20" width="90%" noshade>
	<p class="double-bottom"></p>


	<table class="table table-striped col-sm-8 mx-auto">
		<thead>
			<tr>
				<th scope="col">ログインID</th>
				<th scope="col">ユーザ名</th>
				<th scope="col">生年月日</th>
				<th scope="col"></th>
			</tr>
		</thead>
		<tbody>
<%-- 			c:if test="${userListkey.id != 1}" --%>
			<c:forEach var="user" items="${userListkey管理者以外}">
				<!-- 拡張for文  -->
				<tr>
					<th scope="row">${user.login_id}</th>
					<!-- user.のあとはbeansで定めた形(beansからの戻り値の形？) -->
					<td>${user.name}</td>
					<td>${user.birth_date}</td>

					<td><a href="P1details?id=${user.id }" class="btn0 button btn-primary btn-lg">詳細</a>



					<c:if test="${sessionScope.userInfokey.login_id == 'admin' || user.login_id == sessionScope.userInfokey.login_id}">
<!-- 					<form action="P1update" method="get"> -->
					        <button onclick="location.href='P1update?id=${user.id }'" class="btn1 btn-primary btn-lg">更新</button>
<%--                        <button class="btn1 btn-secondary btn-lg" value='id=${user.id }'>更新</button>  --%>
<%--                        <button type="submit" formaction="P1update?id=${user.id }" class="btn1 btn-secondary btn-lg">更新</button> --%>
<!--                    </form> -->
					</c:if>


					<c:if test="${sessionScope.userInfokey.login_id == 'admin'}">
					    <button onclick="location.href='P1delete?id=${user.id }'" class="btn2 btn-primary btn-lg">削除</button>
					</c:if>
					</td>
				</tr>
			</c:forEach>
<!-- 			/c:if -->
		</tbody>
	</table>




</body>
</html>
