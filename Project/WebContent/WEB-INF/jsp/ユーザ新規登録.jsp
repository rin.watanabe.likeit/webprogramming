 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ユーザ新規登録</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="css/style.css" rel="stylesheet" type="text/css" />

</head>

<body>
    <header>
		<p class="right row">
			${sessionScope.userInfokey.name}さん <a class="red" href="P1LogoutServlet">ログアウト</a>      
		</p>
	</header>


    &nbsp;


    <h1 class="text-center">ユーザ新規登録</h1>
    
    
    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>


    &nbsp;


    <form action="新規登録" method="post">
        <div class="gy col-6 mx-auto">

            <div class="form-group mx-auto row">
                <label class="col-sm-4 col-form-label">ログインID</label>

                <!--div class="w-100 d-none d-md-block"></div  改行-->

                <input name="loginid0" align="right" class="form-control col-sm-4" placeholder="ログインID">
            </div>

            <div class="form-group mx-auto row">
                <label class="col-sm-4 col-form-label">パスワード</label>
                <div align="right">
                    <input name="password0" class="form-control col-sm-12" placeholder="パスワード">
                </div>
            </div>

            <div class="form-group mx-auto row">
                <label class="col-sm-4 col-form-label">パスワード(確認)</label>
                <div align="right">
                    <input name="password02" class="form-control col-sm-13" placeholder="確認用">
                </div>
            </div>


            <div class="form-group mx-auto row">
                <label class="col-sm-4 col-form-label">ユーザ名</label>
                <div align="right">
                    <input name="name0" class="form-control col-sm-14" placeholder="ユーザ名">
                </div>
            </div>

            <div class="form-group mx-auto row">
                <label class="col-sm-4 col-form-label">生年月日</label>
                <div align="right">
                    <input name="date0" class="form-control col-sm-15" placeholder="年/月/日">
                </div>
            </div>

        </div>

    <pre>
    <div class="col-sm-3 mx-auto">
        <button type="submit" class="btn btn-primary col-sm-5">登録</button>
    </div>
    </pre>

 </form>
 

    <div class="col-sm-2" style="text-align:center">
        <a href="Project1userichiranServlet">戻る</a>
    </div>



</body>

</html>
