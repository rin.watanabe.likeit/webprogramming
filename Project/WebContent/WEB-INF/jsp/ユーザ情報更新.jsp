
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザ情報更新</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/style.css" rel="stylesheet" type="text/css" />

</head>

<body>
	<header>
		<p class="right row">
			${sessionScope.userInfokey.name}さん <a class="red" href="P1LogoutServlet">ログアウト</a>
		</p>
	</header>


	&nbsp;


	<h1 class="text-center">ユーザ情報更新</h1>
	
	
	 <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>


	&nbsp;


	<div class="form-group col-sm-9 row">
		<label class="col-sm-8 mx-auto" style="text-align: center">ログインID</label>
		<label class="col-sm-4 mx-auto">${userkeyup.login_id}</label>
	</div>

	<form method="post">

<%-- 		<c:forEach var="user" items="${userkeyup}"> --%>
			<div class="form-group col-sm-9 row">
				<label class="right2 col-sm-8 mx-auto" style="text-align: center">パスワード</label>
				<input name="newPassword" type="text" class="plh2 col-sm-4 mx-auto form-control" placeholder="${userkeyup.password }">
			</div>

			<div class="form-group col-sm-9 row">
				<label class="col-sm-8 mx-auto" style="text-align: center">パスワード(確認)</label>
				<input name="newPassword2" type="text" class="plh2 col-sm-4 mx-auto form-control" placeholder="${userkeyup.password }">
			</div>

			<div class="form-group col-sm-9 row">
				<label class="col-sm-8 mx-auto" style="text-align: center">ユーザ名</label>
				<input name="newName" type="text" class="col-sm-4 mx-auto form-control" value="${userkeyup.name }">
			</div>

			<div class="form-group col-sm-9 row">
				<label class="col-sm-8 mx-auto" style="text-align: center">生年月日</label>
				<input name="newDate" type="text" class="col-sm-4 mx-auto form-control" value="${userkeyup.birth_date }">
			</div>
<%-- 		</c:forEach> --%>


		<pre>
    <div class="col-sm-3 mx-auto">
        <input type="hidden" value="${userkeyup.login_id}" name="loginId">
        <button type="submit" onclick="location.href='P1update?loginId=${userkeyup.login_id}'" class="btn btn-primary col-sm-5">登録</button>     
    </div>
    </pre>

	</form>

	<div class="col-sm-2" style="text-align: center">
		<a href="Project1userichiranServlet">戻る</a>
	</div>


</body>
</html>
