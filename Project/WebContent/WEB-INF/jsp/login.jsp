<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<!--link href="style.css" rel="stylesheet" type="text/css" /-->

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">


<link href="../css/style.css" rel="stylesheet" type="text/css" />



</head>

<body>

	<div style="text-align: center;">
		<h1>ログイン画面</h1>
	</div>


	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>


	<form action="Project1Servlet" method="post">


		&nbsp; <span style="”line-height: 300%;">

			

				<div class="login1 col-sm-10 form-group row mx-auto">

					<label for="exampleInputEmail1" class="col-sm-2 col-form-label">ログインID</label>
					<!--div class="col-sm-5"-->
					<input name="loginId" type="text" style="text-align: center"
						class="col-sm-5" class="form-control" placeholder="ログインID">
					<!--/div-->

				</div>
				<div class="login1 col-sm-10 form-group row mx-auto">

					<label for="exampleInputPassword1" class="col-sm-2 col-form-label">パスワード</label>
					<!--div class="col-sm-5"-->
					<input name="password" type="password" style="text-align: center"
						class="col-sm-5" class="form-control" placeholder="パスワード">
					<!--/div-->

				</div>

		

		</span>



		<pre>
    <div class="col-sm-3 mx-auto">
        <p>
           <button type="submit" class="btn btn-primary col-sm-8">ログイン</button>
        </p>
    </div>
</pre>

	</form>

</body>
</html>
